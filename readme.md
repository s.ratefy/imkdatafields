#ImkDataFields

It's a small library just to use one or many fields reusable in database

It's simple juste clone:
```shell script
composer require ywoume/imkdatafields
```
After that, you can use many Traits in ``entity`` folder.
- `IdTrait`: For adding an id  integer fields
- `EnabledTrait`: For adding an enabled boolean fiels
- `TimeTrait`: For adding an createdAt and  updatedAt datetime fields
- `TokenTmpTrait`: For adding a temporary token string fields 
- `TokenTrait`: For adding a token string fields
- `UserTrait`: For adding a many users linked


#how to use it:

Example in  User entity.

````php
<?php

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User
{
    use IdTrait;

    use EnabledTrait;

    use TokenTmpTrait;

    use TokenTrait;

/// your attribute
````