<?php


namespace Tests\Unit\ImkDataFields\Faker;

use ImkDataFields\Traits\Address\CityTrait;
use ImkDataFields\Traits\Address\CountryTrait;
use ImkDataFields\Traits\Address\PostalCodeTrait;
use ImkDataFields\Traits\Address\StreetTrait;

/**
 * Class AddressFaker
 *
 * @package Tests\Unit\ImkDataFields\Faker
 */
class AddressFaker
{
    use StreetTrait;

    use PostalCodeTrait;

    use CountryTrait;

    use CityTrait;

}
