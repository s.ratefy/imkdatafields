<?php

namespace ImkDataFields\Traits\Enabled;

/**
 * Trait EnabledTrait
 *
 * @package ImkDataFields\Traits\Enabled
 */
trait EnabledTrait
{
    /**
     * @var bool
     */
    private $isEnable = false;

    /**
     * @return bool
     */
    public function getIsEnable(): bool
    {
        return $this->isEnable;
    }

    /**
     * @param bool $isEnable
     */
    public function setIsEnable(bool $isEnable): void
    {
        $this->isEnable = $isEnable;
    }
}
