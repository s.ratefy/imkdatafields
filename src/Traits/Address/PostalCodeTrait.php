<?php

namespace ImkDataFields\Traits\Address;

use Doctrine\ORM\Mapping as ORM;

/**
 * Trait PostalCodeTrait
 *
 * @package ImkDataFields\Traits\Address
 */
trait PostalCodeTrait
{

    /**
     * @var string
     * @ORM\Column(name="postal_code", type="string", length=10, nullable=false)
     */
    private $postalCode;

    /**
     * @return string|null
     */
    public function getPostalCode(): string
    {
        return $this->postalCode;
    }

    /**
     * @param string|null $postalCode
     */
    public function setPostalCode(?string $postalCode): void
    {
        $this->postalCode = $postalCode;
    }
}
