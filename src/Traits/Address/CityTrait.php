<?php


namespace ImkDataFields\Traits\Address;

use Doctrine\ORM\Mapping as ORM;

/**
 * Trait CityTrait
 *
 * @package ImkDataFields\Traits\Address
 */
trait CityTrait
{
    /**
     * @var string|null
     * @ORM\Column(name="city_name", type="string", nullable=false)
     */
    private $cityName;

    /**
     * @return string|null
     */
    public function getCityName(): ?string
    {
        return $this->cityName;
    }

    /**
     * @param string|null $cityName
     */
    public function setCityName(?string $cityName): void
    {
        $this->cityName = $cityName;
    }
}
