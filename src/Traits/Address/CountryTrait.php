<?php

namespace ImkDataFields\Traits\Address;

use Doctrine\ORM\Mapping as ORM;

/**
 * Trait CountryTrait
 *
 * @package ImkDataFields\Traits\Address
 */
trait CountryTrait
{

    /**
     * @var string|null
     * @ORM\Column(name="country", type="string", nullable=true)
     */
    private $country;

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string|null $country
     */
    public function setCountry(?string $country): void
    {
        $this->country = $country;
    }
}
