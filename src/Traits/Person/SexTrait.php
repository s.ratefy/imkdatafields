<?php

namespace ImkDataFields\Traits\Person;

/**
 * Trait SexTrait
 *
 * @package ImkDataFields\Traits\Person
 */
trait SexTrait
{
    /**
     * @var string|null
     */
    private $sex;

    /**
     * @return string|null
     */
    public function getSex(): ?string
    {
        return $this->sex;
    }

    /**
     * @param string|null $sex
     */
    public function setSex(?string $sex): void
    {
        $this->sex = $sex;
    }
}
