<?php

namespace ImkDataFields\Traits\Person;

/**
 * Trait PseudoTrait
 *
 * @package ImkDataFields\Traits\Person
 */
trait PseudoTrait
{
    /**
     * @var string|null
     */
    private $pseudo;

    /**
     * @return string|null
     */
    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }

    /**
     * @param string|null $pseudo
     */
    public function setPseudo(?string $pseudo): void
    {
        $this->pseudo = $pseudo;
    }
}
