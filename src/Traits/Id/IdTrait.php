<?php

namespace ImkDataFields\Traits\Id;

/**
 * Trait IdTrait
 *
 * @package ImkDataFields\Traits\Id
 */
trait IdTrait
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }
}
