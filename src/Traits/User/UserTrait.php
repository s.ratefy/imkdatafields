<?php

namespace ImkDataFields\Traits\User;

/**
 * Trait UserTrait
 *
 * @package ImkDataFields\Traits\User
 */
trait UserTrait
{
    private $user;

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user): self
    {
        $this->user = $user;

        return $this;
    }
}
